#ifndef EOS_GP_H
#define EOS_GP_H

#include "cctk.h"

#define N_INDEPS 1
#define N_DEPS 5

CCTK_INT EOS_GP_SetArray(const CCTK_INT param_table,
                         const CCTK_INT n_elems,
                         const CCTK_POINTER* indep_vars,
                         const CCTK_INT* which_deps_to_set,
                         CCTK_POINTER* dep_vars);

CCTK_INT EOS_GP_Inverse_SetArray(const CCTK_INT param_table,
                                 const CCTK_INT n_elems,
                                 const CCTK_POINTER* indep_vars,
                                 const CCTK_INT* which_deps_to_set,
                                 CCTK_POINTER* dep_vars);

#endif
