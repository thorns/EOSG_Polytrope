#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "EOS_GP.h"

void CCTK_FCALL CCTK_FNAME(EOS_GP_Pressure) (const CCTK_INT* nelems,
                                             const CCTK_REAL* rho,
                                             const CCTK_REAL* press);
void CCTK_FCALL CCTK_FNAME(EOS_GP_IntEn)    (const CCTK_INT* nelems,
                                             const CCTK_REAL* const rho,
                                             const CCTK_REAL* inten);
void CCTK_FCALL CCTK_FNAME(EOS_GP_DPDRho)   (const CCTK_INT* nelems,
                                             const CCTK_REAL* const rho,
                                             const CCTK_REAL* dpdrho);
void CCTK_FCALL CCTK_FNAME(EOS_GP_DPDIE)    (const CCTK_INT* nelems,
                                             const CCTK_REAL* const rho,
                                             const CCTK_REAL* dpdie);
void CCTK_FCALL CCTK_FNAME(EOS_GP_cs2)      (const CCTK_INT* nelems,
                                             const CCTK_REAL* const rho,
                                             const CCTK_REAL* cs2);

CCTK_INT EOS_GP_SetArray(const CCTK_INT param_table,
                         const CCTK_INT n_elems,
                         const CCTK_POINTER* indep_vars,
                         const CCTK_INT* which_deps_to_set,
                         CCTK_POINTER* dep_vars)
{

  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr = 0, var;

  for (var = 0; var < N_DEPS; ++var)
  {
    if (which_deps_to_set[var])
    {
      switch (var)
      {
        case (0):
          CCTK_FNAME(EOS_GP_Pressure) (&n_elems,
                                       (const CCTK_REAL*)indep_vars[0], 
                                       (const CCTK_REAL*)dep_vars[0]);
          break;
        case (1):
          CCTK_FNAME(EOS_GP_IntEn)    (&n_elems,
                                       (const CCTK_REAL*)indep_vars[0], 
                                       (const CCTK_REAL*)dep_vars[1]);
          break;
        case (2):
          CCTK_FNAME(EOS_GP_DPDRho)   (&n_elems,
                                       (const CCTK_REAL*)indep_vars[0], 
                                       (const CCTK_REAL*)dep_vars[2]);
          break;
        case (3):
          CCTK_FNAME(EOS_GP_DPDIE)    (&n_elems,
                                       (const CCTK_REAL*)indep_vars[0], 
                                       (const CCTK_REAL*)dep_vars[3]);
          break;
        case (4):
          CCTK_FNAME(EOS_GP_cs2)      (&n_elems,
                                       (const CCTK_REAL*)indep_vars[0], 
                                       (const CCTK_REAL*)dep_vars[4]);
          break;
      }
    }
  }
  
  return ierr;
}

void CCTK_FCALL CCTK_FNAME(EOS_GP_Inv_Rho)      (const CCTK_INT* nelems,
                                                 const CCTK_REAL* rho,
                                                 const CCTK_REAL* press);
void CCTK_FCALL CCTK_FNAME(EOS_GP_Inv_IntEn)    (const CCTK_INT* nelems,
                                                 const CCTK_REAL* const rho,
                                                 const CCTK_REAL* inten);
void CCTK_FCALL CCTK_FNAME(EOS_GP_Inv_DPDRho)   (const CCTK_INT* nelems,
                                                 const CCTK_REAL* const rho,
                                                 const CCTK_REAL* dpdrho);
void CCTK_FCALL CCTK_FNAME(EOS_GP_Inv_DPDIE)    (const CCTK_INT* nelems,
                                                 const CCTK_REAL* const rho,
                                                 const CCTK_REAL* dpdie);
void CCTK_FCALL CCTK_FNAME(EOS_GP_Inv_cs2)      (const CCTK_INT* nelems,
                                                 const CCTK_REAL* const rho,
                                                 const CCTK_REAL* cs2);

CCTK_INT EOS_GP_Inverse_SetArray(const CCTK_INT param_table,
                                 const CCTK_INT n_elems,
                                 const CCTK_POINTER* indep_vars,
                                 const CCTK_INT* which_deps_to_set,
                                 CCTK_POINTER* dep_vars)
{

  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr = 0, var, dim;

  for (var = 0; var < N_DEPS; ++var)
  {
    if (which_deps_to_set[var])
    {
      switch (var)
      {
        case (0):
          CCTK_FNAME(EOS_GP_Inv_Rho)      (&n_elems,
                                           (const CCTK_REAL*)indep_vars[0], 
                                           (const CCTK_REAL*)dep_vars[0]);
          break;
        case (1):
          CCTK_FNAME(EOS_GP_Inv_IntEn)    (&n_elems,
                                           (const CCTK_REAL*)indep_vars[0], 
                                           (const CCTK_REAL*)dep_vars[1]);
          break;
        case (2):
          CCTK_FNAME(EOS_GP_Inv_DPDRho)   (&n_elems,
                                           (const CCTK_REAL*)indep_vars[0], 
                                           (const CCTK_REAL*)dep_vars[2]);
          break;
        case (3):
          CCTK_FNAME(EOS_GP_Inv_DPDIE)    (&n_elems,
                                           (const CCTK_REAL*)indep_vars[0], 
                                           (const CCTK_REAL*)dep_vars[3]);
          break;
        case (4):
          CCTK_FNAME(EOS_GP_Inv_cs2)      (&n_elems,
                                           (const CCTK_REAL*)indep_vars[0], 
                                           (const CCTK_REAL*)dep_vars[4]);
          break;
      }
    }
  }
  
  return ierr;
}
