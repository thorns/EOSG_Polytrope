#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "util_String.h"
#include "util_ErrorCodes.h"
#include "util_Table.h"

#include "EOS_GP.h"

void EOS_GP_Startup (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INT ierr, table_handle;
  
  table_handle = Util_TableCreate(UTIL_TABLE_FLAGS_CASE_INSENSITIVE);
  
  Util_TableSetInt(table_handle,
                   N_INDEPS,
                   "N independent variables");
  Util_TableSetInt(table_handle,
                   N_DEPS,
                   "N dependent variables");
  Util_TableSetString(table_handle,
                      "Rho",
                      "Independent variable names");
  Util_TableSetString(table_handle,
                      "Pressure SpecificInternalEnergy DPressureDRho DPressureDSpecificInternalEnergy c_s^2",
                      "Dependent variable names");
  Util_TableSetString(table_handle,
                      "Polytrope",
                      "EOS Name");

  ierr = EOS_RegisterCall(table_handle,
                          EOS_GP_SetArray);
  if (ierr)
  {
    CCTK_WARN(0, "Failed to set up EOS_Polytrope call");
  }

  Util_TableSetString(table_handle,
                      "Pressure",
                      "Independent variable names");
  Util_TableSetString(table_handle,
                      "Rho SpecificInternalEnergy DPressureDRho DPressureDSpecificInternalEnergy c_s^2",
                      "Dependent variable names");
  Util_TableSetString(table_handle,
                      "Inverse Polytrope",
                      "EOS Name");

  ierr = EOS_RegisterCall(table_handle,
                          EOS_GP_Inverse_SetArray);
  if (ierr)
  {
    CCTK_WARN(0, "Failed to set up EOS_Polytrope (inverse) call");
  }
}
